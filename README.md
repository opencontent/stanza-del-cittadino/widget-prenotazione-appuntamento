## Quick Overview

Inside the newly created project, you can run some built-in commands:

### `npm install` or `yarn install`

and for start

### `npm start` or `yarn start`

Runs the app in development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will automatically reload if you make changes to the code.<br>
You will see the build errors and lint warnings in the console.

### `npm run build build-no-split` or `yarn build build-no-split`

Builds the app for production to the `build/static` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>

Your app is ready to be deployed.
