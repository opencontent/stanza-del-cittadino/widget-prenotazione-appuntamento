import { Navigate } from "react-router-dom";
import { AppointmentBooking } from "./pages/AppointmentBooking";
import { ThankYouPage } from "./pages/ThankYouPage";

/*
 path: route path
 name: i18n name
 Component: Element React to render
 */
export const routes =  [
  {
    path: "/",
    name: "appointment_booking",
    Component: <AppointmentBooking></AppointmentBooking>
  },
  {
    path: "/prenota_appuntamento/thankyou",
    name: "request_sent",
    Component: <ThankYouPage></ThankYouPage>
  },
  {
    path: "/*",
    name: "appointment_booking",
    Component:
      <Navigate to="/" />
  },
];
