import dayjs from 'dayjs';
import { Badge, Button, Col, Modal, ModalBody, ModalFooter, ModalHeader } from 'design-react-kit';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import shortid from 'shortid';
import { detailPostActions } from '../../_store';
import { ImageHeader } from '../Images/ImageHeader';

export const ModalPopupMarker = ({ show, onClose, properties }) => {
  const key = shortid.generate();
  const dispatch = useDispatch();
  const { detailPost } = useSelector((x) => x.detailPost);

  useEffect(() => {
    if (properties.id) {
      dispatch(detailPostActions.getDetailPost({ id: properties.id })).unwrap();
    }
  }, [dispatch, properties.id]);

  return (
    <Modal isOpen={show} toggle={() => onClose(!show)} align="center" scrollable labelledBy={key}>
      <ModalHeader toggle={() => onClose(!show)} id={key} tag={'h4'}>
        <span className={'modal-title title-small-semi-bold'}>
          Stato della pratica:{' '}
          <Badge pill color={'primary'}>
            {properties.properties.status.name}
          </Badge>
        </span>
      </ModalHeader>
      <ModalBody>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">Titolo</h3>
          <p className="subtitle-small pb-2">{properties.properties.subject}</p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">Tipologia di segnalazione</h3>
          <p className="subtitle-small pb-2">{properties.properties.type.name}</p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">Pubblicazione del</h3>
          <p className="subtitle-small pb-2">
            {dayjs(properties.properties.published_at).format('DD/MM/YYYY  HH:mm')}
          </p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">Indirizzo</h3>
          <p className="subtitle-small pb-2">
            {detailPost.address ? detailPost.address.address : '--'}
          </p>
        </div>
        <div className="border-bottom border-light">
          <h3 className="title-xsmall border-light pt-2">Dettaglio</h3>
          <p className="subtitle-small pb-2">
            {detailPost.description ? detailPost.description : '--'}
          </p>
        </div>
        {detailPost?.images?.length ? (
          <div>
            <h3 className="title-xsmall border-light pt-2">Immagini</h3>
            <Col className="d-lg-flex gap-2 row">
              {detailPost.images.map((img, idx) => (
                <ImageHeader props={img} key={`image-${idx.toString()}`}></ImageHeader>
              ))}
            </Col>
          </div>
        ) : null}
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={() => onClose(!show)} outline className={'w-100'}>
          Chiudi
        </Button>
      </ModalFooter>
    </Modal>
  );
};
