import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { Marker, Popup } from 'react-leaflet';
import { useDispatch } from 'react-redux';
import { detailPostActions } from '../../_store';

export function DraggableMarker(props) {
  const [draggable] = useState(props.draggable);
  const [position, setPosition] = useState(props.position);
  const [address, setAddress] = useState('');
  const markerRef = useRef(null);
  const dispatch = useDispatch();

  const getAddress = useCallback(() => {
    const marker = markerRef.current;
    if (marker != null) {
      setPosition([marker.getLatLng().lat, marker.getLatLng().lng]);
      fetch(
        `https://nominatim.openstreetmap.org/reverse?format=geocodejson&lat=${
          marker.getLatLng().lat
        }&lon=${marker.getLatLng().lng}`
      ).then((response) => {
        response.json().then((data) => {
          setAddress(`${data.features[0].properties.geocoding.label}`);
          dispatch(
            detailPostActions.getAddress({
              address: data.features[0].properties.geocoding.label,
              latitude: marker.getLatLng().lat,
              longitude: marker.getLatLng().lng
            })
          );
          marker.openPopup();
        });
      });
    }
  }, [dispatch]);

  // usual useEffect that'll be triggered on component load, only one time
  useEffect(() => {
    getAddress();
  }, [getAddress]);

  const eventHandlers = useMemo(
    () => ({
      dragend() {
        getAddress();
      },
      click(e) {
        getAddress();
        console.log('marker clicked', e);
      }
    }),
    [getAddress]
  );

  return (
    <Marker
      icon={props.icon}
      draggable={draggable}
      eventHandlers={eventHandlers}
      position={position}
      ref={markerRef}
    >
      {address ? (
        <Popup minWidth={90}>
          <span>{address}</span>
        </Popup>
      ) : null}
    </Marker>
  );
}
