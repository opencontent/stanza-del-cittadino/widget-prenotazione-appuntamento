import { Breadcrumb, BreadcrumbItem } from "design-react-kit";
import { t } from "i18next";
import { useId } from "react";
import { Link } from "react-router-dom";
import useBreadcrumbs from 'use-react-router-breadcrumbs'
import { routes } from "../../routes";

function Breadcrumbs() {
  const breadcrumbs = useBreadcrumbs(routes);
  const id = useId()

  return (
    <div className={'cmp-breadcrumbs'}>
    <Breadcrumb>
      {breadcrumbs.map(({ match, breadcrumb },index) => (
        <BreadcrumbItem key={id + '-'+ index}>
          { (index === 0) ?
            <>
              <a href='/'>{'Homepage'}</a>
              <span className='separator'>/</span>
            </>
             :
         (index !== breadcrumbs.length - 1) ?
              <Link to={match.route.path}>
              {t(match.route.name)}
              { (index !== breadcrumbs.length - 1) ?  <span className='separator'>/</span> : null}
              </Link>

          :  t(match.route.name)
        }
    </BreadcrumbItem>
      ))}
    </Breadcrumb>
    </div>
  );
}

export default Breadcrumbs;
