import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import detectLanguageCustom from './_helpers/detectLanguage';
import en from 'i18n/en.json'
import it from 'i18n/it.json'
import de from 'i18n/de.json'
const languageDetector = new LanguageDetector();
languageDetector.addDetector(detectLanguageCustom);

// the translations
// (tip move them in a JSON file and import them,
// or even better, manage them separated from your code: https://react.i18next.com/guides/multiple-translation-files)

const resources = {
  en: {
    translation: en.bookings
  },
  it: {
    translation: it.bookings
  },
  de: {
    translation: de.bookings
  }
};

i18n
  .use(languageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: 'it',
    interpolation: {
      escapeValue: false // react already safes from xss
    },
    detection: {
      // order and from where user language should be detected
      order: ['custom', 'htmlTag','localStorage'], // keys or params to lookup language from
      lookupLocalStorage: 'defaultLocale',
      lookupSessionStorage: 'defaultLocale'
    }
  });

export default i18n;
