import { getI18n } from "react-i18next";
import { authActions, store } from '../_store/index';

export const fetchWrapper = {
  get: request('GET'),
  getWithCredentials: requestWithCredentials('GET'),
  post: request('POST'),
  put: request('PUT'),
  delete: request('DELETE')
};

function request(method, type) {
  return (url, body) => {
    const requestOptions = {
      method,
      headers: authHeader(url, type)
    };
    if (body) {
      requestOptions.headers['Content-Type'] = 'application/json';
      requestOptions.body = JSON.stringify(body);
    }
    requestOptions.headers['x-locale'] = getI18n().language || 'it';
    return fetch(url, requestOptions).then(handleResponse);
  };
}

function requestWithCredentials(method) {
  return (url, body) => {
    const requestOptions = {
      method,
      credentials: 'include'
    };
    if (body) {
      requestOptions.headers['Content-Type'] = 'application/json';
      requestOptions.body = JSON.stringify(body);
    }
    return fetch(url, requestOptions).then(handleResponse);
  };
}

// helper functions

function authHeader(url, type) {
  // return auth header with jwt if user is logged in and request is to the api url
  const token = authToken();
  const isLoggedIn = !!token;
  const isApiUrl = url.includes('/api');
  if (isLoggedIn && isApiUrl) {
    return { Authorization: `Bearer ${token}`, Accept: 'application/json' };
  } else {
    return { Accept: 'application/json' };
  }
}

function authToken() {
  return store.getState().auth.token || process.env.REACT_APP_DEV_TOKEN;
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);

    if (!response.ok) {
      if ([401, 403, 500].includes(response.status) && authToken()) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        //const logout = () => store.dispatch(authActions.logout());
        //logout();
      }else if([500,501, 502, 503, 504].includes(response.status)){
        store.dispatch(authActions.error(true))
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
