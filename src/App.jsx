import { useEffect, useLayoutEffect } from 'react';
import { Routes, Route, Navigate, useLocation } from 'react-router-dom';
import { ErrorBoundary } from 'react-error-boundary';

import { AppointmentBooking } from './pages/AppointmentBooking';
import { ThankYouPage } from './pages/ThankYouPage';

//Set globally BTS-ITALIA Version
import BOOTSTRAP_ITALIA_VERSION from 'bootstrap-italia/dist/version';
import { loadFonts } from 'bootstrap-italia';
import { useSelector } from 'react-redux';
import { Errors } from './pages/Errors';

window.BOOTSTRAP_ITALIA_VERSION = BOOTSTRAP_ITALIA_VERSION;

export function App() {
  const location = useLocation();
  const { error } = useSelector((x) => x.auth);

  useEffect(() => {
    // Importing files depending on env
    if (process.env.REACT_APP_STYLE === 'true') {
      import(`./assets/stylesheets/core.scss`);
      import('./index.scss');
      loadFonts('https://static.opencityitalia.it/fonts');
    } else {
      import('./index.scss');
    }
  }, []);

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  return (
    <div>
      {error ? (
        <ErrorBoundary
          FallbackComponent={Errors}
          fallback={<Errors error={{ name: 'ErrorBoundary' }}></Errors>}>
          <Errors error={{ name: 'ErrorBoundary' }}></Errors>
        </ErrorBoundary>
      ) : (
    <Routes>
      <Route path="/" element={<AppointmentBooking />} />
      <Route path="/prenota_appuntamento/thankyou" element={<ThankYouPage />} />
      <Route path="/*" element={<Navigate to="/" />} />
    </Routes>
  )}
    </div>
  );
}
