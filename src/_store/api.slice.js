import { createAsyncThunk, createSlice, current } from '@reduxjs/toolkit';

import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice

const name = 'api';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      /** getCalendars **/
      .addCase(`${name}/getCalendars/pending`, (state, { meta }) => {
        state.calendars = { loading: true };
      })
      .addCase(`${name}/getCalendars/fulfilled`, (state, action) => {
        state.calendars = action.payload;
      })
      .addCase(`${name}/getCalendars/rejected`, (state, action) => {
        state.calendars = { error: action.error };
      })
      /** getCalendarById **/
    .addCase(`${name}/getCalendarById/fulfilled`, (state, action) => {
      state.is_moderated = action.payload.is_moderated;
    })
      /** getUserGroups **/
      .addCase(`${name}/getUserGroups/fulfilled`, (state, action) => {
        state.offices = action.payload;
      })
      .addCase(`${name}/getUserGroups/rejected`, (state, action) => {
        state.offices = { error: action.error };
      })

      /** getUserGroupsServiceId **/
      .addCase(`${name}/getUserGroupsServiceId/fulfilled`, (state, action) => {
        state.offices = action.payload;
      })
      .addCase(`${name}/getUserGroupsServiceId/rejected`, (state, action) => {
        state.offices = { error: action.error };
      })

      /** getUserGroupById **/
      .addCase(`${name}/getUserGroupById/fulfilled`, (state, action) => {
        state.offices = [action.payload];
      })
      /** confirmMeeting **/
      .addCase(`${name}/confirmMeeting/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/confirmMeeting/rejected`, (state, action) => {
        state.application = { error: action.error };
      })
      /** getCalendarAvailabilities **/
      .addCase(`${name}/getCalendarAvailabilities/fulfilled`, (state, action) => {
        state.availabilities = action.payload;
      })
      /** getCalendarAvailabilitiesTime **/
      .addCase(`${name}/getCalendarAvailabilitiesTime/fulfilled`, (state, action) => {
        state.times = action.payload;
      })
      /** newMeeting **/
      .addCase(`${name}/newMeeting/fulfilled`, (state, action) => {
        state.meeting = action.payload;
      })
      .addCase(`${name}/newMeeting/rejected`, (state, action) => {
        state.meeting = { error: action.error };
      })
      /** getOpeningHours **/
      .addCase(`${name}/getOpeningHours/pending`, (state, { meta }) => {
        state.opening_hour = { loading: true };
      })
      .addCase(`${name}/getOpeningHours/fulfilled`, (state, action) => {
        state.opening_hour = action.payload;
      })
      .addCase(`${name}/getOpeningHours/rejected`, (state, action) => {
        state.opening_hour = { error: action.error };
      })
      /** getOpeningHourById **/
      .addCase(`${name}/getOpeningHourById/pending`, (state, { meta }) => {
        state.opening_hour = { loading: true };
      })
      .addCase(`${name}/getOpeningHourById/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.opening_hour = action.payload;
        state.is_moderated = currentState.is_moderated ? currentState.is_moderated : action.payload.is_moderated;
    })
      .addCase(`${name}/getOpeningHourById/rejected`, (state, action) => {
        state.opening_hour = { error: action.error };
      })
      /** getServicesByUserGroups **/
      .addCase(`${name}/getServicesByUserGroups/pending`, (state, { meta }) => {
        state.services = { loading: true };
      })
      .addCase(`${name}/getServicesByUserGroups/fulfilled`, (state, action) => {
        state.services = action.payload;
      })
      .addCase(`${name}/getServicesByUserGroups/rejected`, (state, action) => {
        state.services = { error: action.error };
      })
      /**  getCalendarMonths **/
      .addCase(`${name}/getCalendarMonths/fulfilled`, (state, action) => {
        state.months = action.payload;
      })
      /** getServiceBookings **/
      .addCase(`${name}/getServiceBookings/fulfilled`, (state, action) => {
        state.service_id = action.payload;
      })

      /** getDraftApplication **/
      .addCase(`${name}/getDraftApplication/fulfilled`, (state, action) => {
        const response = action.payload;
        state.application = response.data.length ? response.data[0] : {};
      })
      .addCase(`${name}/getDraftApplication/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

      /** tenantInfo **/
      .addCase(`${name}/tenantInfo/fulfilled`, (state, action) => {
        state.meta = action.payload;
      })

      /** updateApplication **/
      .addCase(`${name}/updateApplication/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.application = currentState.application;
      })
      .addCase(`${name}/updateApplication/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

    /** updateApplication **/
  .addCase(`${name}/getServicesById/fulfilled`, (state, action) => {
      const currentState = current(state);
      state.serviceObj = currentState.serviceObj;
    })
    .addCase(`${name}/getServicesById/rejected`, (state, action) => {
      state.serviceObj = { error: action.error };
    });

  }
});

// exports
export const apiActions = { ...slice.actions, ...extraActions };
export const apiReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    availabilities: [],
    calendars: [],
    times: [],
    meeting: {},
    application: {},
    opening_hour: {
      days_of_week: []
    },
    offices: [],
    services: [],
    months: [],
    service_id: null,
    serviceObj: null,
    meta: {},
    is_moderated: false
  };
}

function createExtraActions() {
  const baseUrl = window.OC_BASE_URL || `${process.env.REACT_APP_API_URL}`;

  return {
    getOtherPosts: getOtherPosts(),
    getCalendars: getCalendars(),
    getCalendarAvailabilities: getCalendarAvailabilities(),
    getCalendarAvailabilitiesTime: getCalendarAvailabilitiesTime(),
    newMeeting: newMeeting(),
    confirmMeeting: confirmMeeting(),
    getOpeningHours: getOpeningHours(),
    getOpeningHourById: getOpeningHourById(),
    getUserGroups: getUserGroups(),
    getCalendarById: getCalendarById(),
    getServicesByUserGroups: getServicesByUserGroups(),
    getServicesById: getServicesById(),
    getCalendarMonths: getCalendarMonths(),
    getServiceBookings: getServiceBookings(),
    getUserGroupById: getUserGroupById(),
    getUserGroupsServiceId: getUserGroupsServiceId(),
    getDraftApplication: getDraftApplication(),
    updateApplication: updateApplication(),
    tenantInfo: tenantInfo()
  };

  function confirmMeeting() {
    return createAsyncThunk(
      `${name}/confirmMeeting`,
      async (data) => await fetchWrapper.post(`${baseUrl}/api/applications`, data)
    );
  }

  function tenantInfo() {
    return createAsyncThunk(
      `${name}/tenantInfo`,
      async () => await fetchWrapper.get(`${baseUrl}/api/tenants/info`)
    );
  }

  function getServiceBookings() {
    return createAsyncThunk(
      `${name}/getServiceBookings`,
      async () => await fetchWrapper.get(`${baseUrl}/api/services/bookings`)
    );
  }

  function getCalendars() {
    return createAsyncThunk(
      `${name}/getCalendars`,
      async () => await fetchWrapper.get(`${baseUrl}/api/calendars?type=time_fixed_slots`)
    );
  }

  function getCalendarById() {
    return createAsyncThunk(
      `${name}/getCalendarById`,
      async (params) => await fetchWrapper.get(`${baseUrl}/api/calendars/${params.calendar_id}`)
    );
  }

  function getUserGroups() {
    return createAsyncThunk(
      `${name}/getUserGroups`,
      async (params) => await fetchWrapper.get(`${baseUrl}/api/user-groups?has_calendar=true`)
    );
  }

  function getUserGroupsServiceId() {
    return createAsyncThunk(
      `${name}/getUserGroups`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/api/user-groups?has_calendar=true&service_id=${params.service_id}`
        )
    );
  }

  function getUserGroupById() {
    return createAsyncThunk(
      `${name}/getUserGroupById`,
      async (params) => await fetchWrapper.get(`${baseUrl}/api/user-groups/${params.id}`)
    );
  }

  function getServicesById() {
    return createAsyncThunk(
      `${name}/getServicesById`,
      async (params) => await fetchWrapper.get(`${baseUrl}/api/services/${params.id}`)
    );
  }

  function getOtherPosts() {
    return createAsyncThunk(
      `${name}/getOtherPosts`,
      async (arg) => await fetchWrapper.get(`${arg.next}`)
    );
  }

  function getCalendarAvailabilities() {
    return createAsyncThunk(
      `${name}/getCalendarAvailabilities`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/api/calendars/${params.id}/availabilities?from_time=${params.from_time}&to_time=${params.to_time}&available=true`
        )
    );
  }

  function getCalendarAvailabilitiesTime() {
    return createAsyncThunk(
      `${name}/getCalendarAvailabilitiesTime`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/api/calendars/${params.calendar_id}/availabilities/${params.day}?available=true`
        )
    );
  }

  function getCalendarMonths() {
    return createAsyncThunk(
      `${name}/getCalendarMonths`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/api/calendars/${params.calendar_id}/availabilities?available=true`
        )
    );
  }

  function getOpeningHours() {
    return createAsyncThunk(
      `${name}/getOpeningHours`,
      async (params) =>
        await fetchWrapper.get(`${baseUrl}/api/calendars/${params.calendar_id}/opening-hours`)
    );
  }

  function getOpeningHourById() {
    return createAsyncThunk(
      `${name}/getOpeningHourById`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/api/calendars/${params.calendar_id}/opening-hours/${params.id}`
        )
    );
  }

  function newMeeting() {
    return createAsyncThunk(
      `${name}/newMeeting`,
      async (data) => await fetchWrapper.post(`${baseUrl}/it/meetings/new-draft`, data)
    );
  }

  function getServicesByUserGroups() {
    return createAsyncThunk(
      `${name}/getServicesByUserGroups`,
      async (params) =>
        await fetchWrapper.get(`${baseUrl}/api/services?user_group_ids=${params.user_group_id}`)
    );
  }

  function getDraftApplication() {
    return createAsyncThunk(
      `${name}/getDraftApplication`,
      async () =>
        await fetchWrapper.get(
          `${baseUrl}/api/applications?status=1000&limit=1&sort=desc&version=2&service_identifier=bookings`
        )
    );
  }

  function updateApplication() {
    return createAsyncThunk(
      `${name}/updateApplication`,
      async (params) =>
        await fetchWrapper.put(`${baseUrl}/api/applications/${params.id}`, params.data)
    );
  }
}
