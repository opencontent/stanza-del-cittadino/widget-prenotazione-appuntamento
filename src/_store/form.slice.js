import { createSlice } from '@reduxjs/toolkit';

const rootSlice = createSlice({
  name: 'form',
  initialState: {
    name: '',
    surname: '',
    email_address: '',
    phone_number: '',
    fiscal_code: '',
    office: null,
    day: '',
    service: '',
    meeting_id: '',
    calendar: 'DD/MM/YYYY @ start_time-end_time (calendar_id#meeting_id#opening_hour_id)',
    user_message: '',
    time: {
      date: '',
      start_time: '',
      end_time: '',
      slots_available: 1,
      opening_hour: '',
      availability: true
    },
    month: '',
    opening_hour: '',
    location: '',
    privacy: false
  },
  reducers: {
    //calendar_id
    storeOffice: (state, action) => {
      state.office = action.payload;
    },
    storeDay: (state, action) => {
      state.day = action.payload;
    },
    storeTime: (state, action) => {
      state.time = action.payload;
    },
    storeService: (state, action) => {
      state.service = action.payload;
    },
    storeUserMessage: (state, action) => {
      state.user_message = action.payload;
    },
    storeCalendar: (state, action) => {
      state.calendar = action.payload;
    },
    storeName: (state, action) => {
      state.name = action.payload;
    },
    storeSurname: (state, action) => {
      state.surname = action.payload;
    },
    storeEmail: (state, action) => {
      state.email_address = action.payload;
    },
    storePhoneNumber: (state, action) => {
      state.phone_number = action.payload;
    },
    storeFiscalCode: (state, action) => {
      state.fiscal_code = action.payload;
    },
    storeMonth: (state, action) => {
      state.month = action.payload;
    },
    storeOpeningHours: (state, action) => {
      state.opening_hour = action.payload;
    },
    storeLocation: (state, action) => {
      state.location = action.payload;
    },
    storeApplication: (state, action) => {
      state.application = action.payload;
    },
    storePrivacy: (state, action) => {
      state.privacy = action.payload;
    },
    storeReset: (state, action) => {
      state.name = ''
      state.surname= ''
        state.email_address= ''
        state.phone_number= ''
        state.fiscal_code= ''
        state.day= ''
        state.service= ''
        state.meeting_id= ''
        state.calendar = null
        state.user_message= ''
        state.time= {
        date: '',
          start_time: '',
          end_time: '',
          slots_available: 1,
          opening_hour: '',
          availability: true
      }
      state.month = ''
        state.opening_hour= ''
        state.location= ''
        state.privacy= false
    },
  }
});

export const formReducer = rootSlice.reducer;

export const {
  storeDay,
  storeOffice,
  storeOpeningHours,
  storeTime,
  storeService,
  storeUserMessage,
  storeCalendar,
  storeName,
  storeSurname,
  storePhoneNumber,
  storeFiscalCode,
  storeEmail,
  storeMonth,
  storeLocation,
  storeApplication,
  storePrivacy,
  storeReset
} = rootSlice.actions;
