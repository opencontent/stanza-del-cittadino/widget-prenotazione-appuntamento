import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

const checkAnonymUser = (email) => {
  const re =
    /anonymous.fake.email/gm;
  return re.test(email);
}

// create slice

const name = 'currentUser';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(`${name}/getCurrentUser/pending`, (state, { meta }) => {
        state.loading = true;
      })
      .addCase(`${name}/getCurrentUser/fulfilled`, (state, action) => {
        state.currentUser = action.payload.length ? action.payload[0] : [];
        state.currentUser.isAnonym = checkAnonymUser(state.currentUser.email)
        state.loading = false;
      })
      .addCase(`${name}/getUserById/pending`, (state, { meta }) => {
        state.loading = true;
      })
      .addCase(`${name}/getUserById/fulfilled`, (state, action) => {
        state.currentUser = action.payload;
      state.currentUser.isAnonym = checkAnonymUser(state.currentUser.email)
        state.loading = false;
      });
  }
});

// exports
export const currentUserActions = { ...slice.actions, ...extraActions };
export const currentUserReducer = slice.reducer;

// implementation
function createInitialState() {
  return {
    currentUser: {},
    loading: false
  };
}

function createExtraActions() {
  const baseUrl = window.OC_BASE_URL || `${process.env.REACT_APP_API_URL}`;

  return {
    getCurrentUser: getCurrentUser(),
    getUserById: getUserById()
  };

  function getCurrentUser() {
    return createAsyncThunk(
      `${name}/getCurrentUser`,
      async () => await fetchWrapper.get(`${baseUrl}/api/users`)
    );
  }

  function getUserById() {
    return createAsyncThunk(
      `${name}/getUserById`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/api/users/${arg.id}`)
    );
  }
}
