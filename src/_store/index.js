import { configureStore } from '@reduxjs/toolkit';
import { authReducer } from './auth.slice';
import { formReducer } from './form.slice';
import { apiReducer } from './api.slice';
import { currentUserReducer } from './currentuser.slice';

export * from './auth.slice';
export * from './api.slice';
export * from './currentuser.slice';
export * from './form.slice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    api: apiReducer,
    currentUser: currentUserReducer,
    form: formReducer
  }
  // here we restore the previously persisted state
  //preloadedState: loadState(),
});
