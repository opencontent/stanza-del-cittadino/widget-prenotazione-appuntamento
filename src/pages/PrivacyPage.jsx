import { Button, Input, Label, Row } from 'design-react-kit';
import { useEffect, useLayoutEffect } from "react";
import { Controller, useForm } from 'react-hook-form';

import { Trans, useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { Link, useLocation, useNavigate } from "react-router-dom";
import { apiActions, storePrivacy } from '../_store';

export { PrivacyPage };

function PrivacyPage({ onClick, activeStep }) {
  const { t } = useTranslation();
  const { privacy } = useSelector((x) => x.form);
  const { application } = useSelector((x) => x.api);
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();

  const {
    handleSubmit,
    formState: { isDirty, isValid },
    control,
    setValue
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      privacy: false
    }
  });

  const onSubmit = (data) => {
    dispatch(storePrivacy(data.privacy));
    onClick(activeStep + 1);
  };

  const isTrue = (value) => value || t('required_field');

  // get draft application
  useEffect(() => {
    if (application?.data) {
      if (application?.data?.hasOwnProperty('privacy')) {
        setValue('privacy', application?.data?.privacy, {
          shouldValidate: true,
          shouldDirty: true
        });
      }
    }
  }, [setValue, application.data]);

  useEffect(() => {
    if (privacy) {
      setValue('privacy', privacy, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
  }, [privacy, setValue]);

  useEffect(() => {
    dispatch(apiActions.tenantInfo());
  }, [dispatch]);

  useEffect(() => {
    const p= window.location.href.indexOf("#");
    if(p<0){
      console.log('service_id',window.location.search)
      sessionStorage.setItem('service_id',window.location.search)
      navigate("/"+window.location.search);
    }
  })

  const handleChange = (event) => {
    dispatch(storePrivacy(event.target.checked));
  };

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Row className="justify-content-center">
        <div className="col-12 col-lg-8 pb-40 pb-lg-80">
          <p className="text-paragraph mt-4 mb-0 mt-md-40 mb-lg-40">
            <Trans
              t={t}
              values={{
                url: window.OC_PRIVACY_URL
              }}
              i18nKey={'privacy_description'}
              components={{ Link: <Link /> }}
            ></Trans>
          </p>

          <div className="form-check mt-4 mb-3 mt-md-40 mb-lg-40">
            <div className="checkbox-body d-flex align-items-center">
              <Controller
                name="privacy"
                control={control}
                rules={{ required: false, validate: isTrue }}
                render={({ field }) => (
                  <>
                    <Input
                      id="privacy"
                      className={''}
                      placeholder={''}
                      invalid={!isValid ? true : false}
                      innerRef={field.ref}
                      value={field.value}
                      onChange={(event) => {
                        field.onChange(event);
                        handleChange(event);
                      }}
                      type="checkbox"
                      checked={privacy}
                      aria-label={t('privacy_label')}
                    />
                    <Label
                      className="title-small-semi-bold pt-1 active"
                      htmlFor="privacy"
                      id={'privacyDescription'}
                    >
                      {t('privacy_label')}
                    </Label>
                  </>
                )}
              />
            </div>
          </div>
          <Button
            color="primary"
            className="mobile-full"
            type="submit"
            disabled={!isValid || !isDirty}
          >
            <span className=""> {t('next')}</span>
          </Button>
        </div>
      </Row>
    </form>
  );
}
