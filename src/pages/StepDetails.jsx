import { Button, Container, Icon, Row, TextArea } from 'design-react-kit';
import { t } from 'i18next';
import { useEffect, useLayoutEffect } from "react";
import { Controller, useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useSearchParams } from "react-router-dom";
import styled from 'styled-components';

import { apiActions, storeService, storeUserMessage } from '../_store';

export { StepDetails };

function StepDetails({ onClick, registerDetails, registerReason, activeStep }) {
  const dispatch = useDispatch();
  const { service, user_message, time, office } = useSelector((x) => x.form);
  const { meeting, services, application, serviceObj } = useSelector((x) => x.api);
  const [searchParams] = useSearchParams();
  const location = useLocation();
  const {
    handleSubmit,
    formState: { errors, isDirty, isValid, isSubmitting },
    control,
    setValue
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      service: '',
      user_message: ''
    }
  });

  // get draft application
  useEffect(() => {
    if (application?.data) {
      if (application?.data?.hasOwnProperty('user_message')) {
        setValue('user_message', application?.data?.user_message, {
          shouldValidate: true,
          shouldDirty: true
        });
      }

      if (application?.data?.hasOwnProperty('service')) {
        setValue('service', application?.data?.service, {
          shouldValidate: true,
          shouldDirty: true
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setValue, application.data]);

  const onSubmit = (data) => {
    if (isSubmitting) {
      return;
    }
    dispatch(storeService(data.service));
    dispatch(storeUserMessage(data.user_message));

    const meetingBooking = {
      date: time.date,
      slot: time.start_time + '-' + time.end_time,
      opening_hour: time.opening_hour,
      calendar: office.calendar_id,
      meeting: meeting?.id
    };
    dispatch(apiActions.newMeeting(meetingBooking));
    onClick(activeStep + 1);
  };

  const onPrev = () => {
    onClick(activeStep - 1);
  };

  useEffect(() => {
    if(office?.id){
      dispatch(apiActions.getServicesByUserGroups({ user_group_id: office.id }));
    }
  }, [dispatch, office?.id]);

  useEffect(() => {
    if (service) {
      setValue('service', service, {
        shouldValidate: true,
        shouldDirty: true
      });
    } else if (searchParams.get('service_id') && services.length > 0) {
      dispatch(apiActions.getServicesById({ id: searchParams.get('service_id') })).unwrap().then(response =>{
        let cloneOffices = {
          ...services.filter((el) => el.id === searchParams.get('service_id'))[0]
        };
        setValue('service', cloneOffices?.name || response.name, {
          shouldValidate: true,
          shouldDirty: true
        });
      });

    }

    if (user_message) {
      setValue('user_message', user_message, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [service, user_message, setValue, searchParams, services]);

  const handleChangeService = (event) => {
    dispatch(storeService(event.target.value));
  };

  const handleChangeUserMessages = (event) => {
    dispatch(storeUserMessage(event.target.value));
  };

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

  return (
    <Container>
      <Row>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="steppers-content" aria-live="polite">
            <div className="it-page-sections-container">
              <section className="it-page-section" id="reason" {...registerReason}>
                <div className="cmp-card">
                  <div className="card has-bkg-grey shadow-sm p-big">
                    <div className="card-header border-0 p-0 mb-lg-30">
                      <div className="d-flex">
                        <h2 className="title-xxlarge mb-0"> {t('reason')}*</h2>
                      </div>
                      <p className="subtitle-small mb-0">{t('chose_reason_appointment')}</p>
                    </div>
                    <div className="card-body p-0">
                      <div className="cmp-text-area p-0">
                        <Controller
                          name="service"
                          control={control}
                          rules={{
                            required: {
                              value: true,
                              message: t('required_field')
                            },
                            maxLength: {
                              value: 200,
                              message: t('enter_max_200_char')
                            }
                          }}
                          render={({ field }) => (
                            <WrapperClassTextArea>
                              <TextArea
                                id="service"
                                className={'text-area'}
                                aria-labelledby={'details_description'}
                                label={t('details_description')}
                                wrapperClassName={'custom-label'}
                                style={{ paddingTop: '.375rem',width: '100%' }}
                                readOnly={searchParams.get('service_id') || serviceObj?.name}
                                invalid={
                                  errors.service?.type === 'required' ||
                                  errors.service?.type === 'maxLength'
                                    ? true
                                    : false
                                }
                                validationText={errors.service?.message}

                                innerRef={field.ref}
                                value={field.value}
                                onChange={(event) => {
                                  field.onChange(event);
                                  handleChangeService(event);
                                }}
                                rows={3}
                              />
                            </WrapperClassTextArea>
                          )}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section className="it-page-section" id="details" {...registerDetails}>
                <div className="cmp-card">
                  <div className="card has-bkg-grey shadow-sm p-big">
                    <div className="card-header border-0 p-0 mb-lg-30 m-0">
                      <div className="d-flex">
                        <h2 className="title-xxlarge mb-0">{t('details')}*</h2>
                      </div>
                      <p className="subtitle-small mb-0 mb-3" id={'details_description'}>
                        {t('details_description')}
                      </p>
                    </div>
                    <div className="card-body p-0">
                      <div className="cmp-text-area p-0">
                        <Controller
                          name="user_message"
                          control={control}
                          rules={{
                            required: {
                              value: true,
                              message: t('required_field')
                             },
                            maxLength: {
                              value: 200,
                              message: t('enter_max_200_char')
                            }
                          }}
                          render={({ field }) => (
                            <WrapperClassTextArea>
                              <TextArea
                                id="user_message"
                                className={'text-area'}
                                aria-labelledby={'details_description'}
                                label={t('details_description')}
                                wrapperClassName={'custom-label'}
                                style={{ paddingTop: '.375rem',width: '100%' }}
                                invalid={
                                  errors.user_message?.type === 'required' ||
                                  errors.user_message?.type === 'maxLength'
                                    ? true
                                    : false
                                }
                                validationText={errors.user_message?.message}

                                innerRef={field.ref}
                                value={field.value}
                                onChange={(event) => {
                                  field.onChange(event);
                                  handleChangeUserMessages(event);
                                }}
                                rows={3}
                              />
                            </WrapperClassTextArea>
                          )}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
          <div className="cmp-nav-steps">
            <nav
              className="steppers-nav"
              aria-label="Step"
              style={{
                display: 'flex',
                justifyContent: 'space-between'
              }}>
              <Button type="button" size={'sm'} className="steppers-btn-prev p-0" onClick={onPrev}>
                <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
                <span className="text-button-sm">{t('back')}</span>
              </Button>
              <Button
                color="primary"
                type="submit"
                size={'sm'}
                className="steppers-btn-confirm"
                disabled={!isValid || !isDirty}>
                <span className="text-button-sm">{t('next')}</span>
                <Icon
                  icon={'it-chevron-right'}
                  size={'sm'}
                  color={'primary'}
                  className={'icon-white'}></Icon>
              </Button>
            </nav>
          </div>
        </form>
      </Row>
    </Container>
  );
}

export const WrapperClassTextArea = styled.div`
  .custom-label {
    //same visually-hidden
    label {
      position: absolute !important;
      width: 1px !important;
      height: 1px !important;
      padding: 0 !important;
      margin: -1px !important;
      overflow: hidden !important;
      clip: rect(0, 0, 0, 0) !important;
      white-space: nowrap !important;
      border: 0 !important;
    }
    small {
      color: #455a64;
    }
    .form-feedback.just-validate-error-label {
      color: #ce2740;
    }
  }
    #service:read-only {
        background: lightgray;
    }
`;
