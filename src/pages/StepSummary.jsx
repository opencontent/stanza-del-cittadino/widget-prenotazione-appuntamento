import {
  Button, Callout, CalloutText, CalloutTitle,
  Container,
  Icon,
  NotificationManager,
  notify,
  Row
} from "design-react-kit";
import { t } from 'i18next';
import { useEffect, useLayoutEffect, useState } from "react";
import { getI18n } from "react-i18next";
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { getUUID } from "../_helpers/utilities";
import { apiActions, storeApplication, storeReset } from "../_store";
import dayjs from 'dayjs';

require('dayjs/locale/it');
dayjs.locale('it');

export { StepSummary };

function StepSummary({ activeStep, onClick }) {
  const dispatch = useDispatch();
  const locationWindow = useLocation();
  const {
    name,
    surname,
    fiscal_code,
    phone_number,
    email_address,
    time,
    user_message,
    office,
    service,
    location,
    privacy
  } = useSelector((x) => x.form);
  const { meeting, application, service_id } = useSelector((x) => x.api);
  const { currentUser } = useSelector((x) => x.currentUser);
  const navigate = useNavigate();
  // this determines whether the button is disabled or not
  const [isDisabled, setIsDisabled] = useState(false);

  useEffect(() => {
    dispatch(apiActions.getServiceBookings());
  }, [dispatch]);

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [locationWindow.pathname]);

  const onSubmit = (data) => {
    setIsDisabled(true);
    const getId =
      application?.id || application?.data?.length
        ? application?.id || application?.data[0]?.id
        : null;
    const dataApplication = {
      ...(getId && { id: getId }),
      service: service_id?.identifier,
      data: {
        applicant: {
          data: {
            email_address: email_address,
            phone_number: phone_number,
            completename: {
              data: {
                name: name,
                surname: surname
              }
            },
            fiscal_code: {
              data: {
                fiscal_code: fiscal_code && getUUID(fiscal_code) ? '' : fiscal_code
              }
            }
          }
        },
        calendar: `${dayjs(time.date).format('DD/MM/YYYY')} @ ${time.start_time}-${
          time.end_time
        } (${office.calendar_id}#${meeting.id}#${time.opening_hour})`,
        user_group: {
          id: office.id,
          name: office.name,
          calendar_id: office.calendar_id
        },
        day: time.date,
        meeting_id: meeting.id,
        service: service,
        time: {
          availability: true,
          date: time.date,
          end_time: time.end_time,
          opening_hour: time.opening_hour,
          slots_available: 1,
          start_time: time.start_time
        },
        user_message: user_message,
        privacy: privacy
      },
      status: data.status
    };

    dispatch(storeApplication(dataApplication));

    if (dataApplication.status === '1900') {
      if (dataApplication?.id) {
        return dispatch(
          apiActions.updateApplication({
            data: dataApplication,
            id: dataApplication.id
          })
        ).then((response) => {
          if (response?.payload?.length) {
            navigate(`/prenota_appuntamento/thankyou`);
            dispatch(storeReset())
          }
        });
      } else {
        return dispatch(apiActions.confirmMeeting(dataApplication)).then((response) => {
          if (response?.payload?.data) {
            navigate(`/prenota_appuntamento/thankyou`);
            dispatch(storeReset())
          }
        });
      }
    } else if (dataApplication.status === '1000') {
      if (dataApplication?.id) {
        return dispatch(
          apiActions.updateApplication({
            data: dataApplication,
            id: dataApplication.id
          })
        ).then(() => {
          setIsDisabled(false);
          notify(t('request_saved_successfully'), {
            dismissable: true,
            state: 'success',
            duration: 3000
          });
        });
      } else {
        return dispatch(apiActions.confirmMeeting(dataApplication)).then(() => {
          setIsDisabled(false);
          notify(t('request_saved_successfully'), {
            dismissable: true,
            state: 'success',
            duration: 3000
          });
        });
      }
    }
    setIsDisabled(false);
  };

  const saveApplication = () => {
    onSubmit({ status: '1000' });
  };

  const onPrev = () => {
    onClick(activeStep - 1);
  };

  const onChangePage = (e) => {
    onClick(e);
  };

  return (
    <Container>
      <Row>
        <div className="steppers-content" aria-live="polite">
          <div className="it-page-sections-container">
            <WrapperCallout>
            <Callout color="warning" highlight className={'mb-5'}>
              <CalloutTitle>
                <Icon aria-hidden icon="it-help-circle" />
                <span>{t('warning')}</span>
              </CalloutTitle>
              <CalloutText className='titillium text-paragraph'>
                  {t('alert_info_post')}
                  <span className="d-lg-block"> {t('alert_info_post_verify')}</span>
              </CalloutText>
            </Callout>
            </WrapperCallout>
            <div className="mt-2">
              <h2 className="visually-hidden">{t('details_appointment')}</h2>
              <div className="cmp-card mb-4">
                <div className="card has-bkg-grey shadow-sm mb-0">
                  <div className="card-body p-0">
                    <div className="cmp-info-summary bg-white mb-3 mb-lg-4 p-3 p-lg-4">
                      <div className="card">
                        <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                          <h4 className="title-large-semi-bold mb-3">{t('office')}</h4>
                          <Button className="text-decoration-none" onClick={() => onChangePage(2)}>
                            {t('edit')}
                          </Button>
                        </div>

                        <div className="card-body p-0">
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('type_office')}</div>
                            <div className="border-light">
                              <p className="data-text">{office.name}</p>
                            </div>
                          </div>
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('municipality')}</div>
                            <div className="border-light">
                              <p
                                className="data-text"
                                dangerouslySetInnerHTML={{ __html: location }}></p>
                            </div>
                          </div>
                        </div>
                        <div className="card-footer p-0"></div>
                      </div>
                    </div>
                    <div className="cmp-info-summary bg-white mb-3 mb-lg-4 p-3 p-lg-4">
                      <div className="card">
                        <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                          <h4 className="title-large-semi-bold mb-3">{t('date_and_time')}</h4>
                          <Button className="text-decoration-none" onClick={() => onChangePage(3)}>
                            {t('edit')}
                          </Button>
                        </div>

                        <div className="card-body p-0">
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('date')}</div>
                            <div className="border-light">
                              <p className="data-text">
                                {dayjs(time.date).locale(getI18n().language).format('dddd DD MMMM YYYY')}
                              </p>
                            </div>
                          </div>
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('hour')}</div>
                            <div className="border-light">
                              <p className="data-text">
                                {time.start_time} - {time.end_time}
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="card-footer p-0"></div>
                      </div>
                    </div>
                    <div className="cmp-info-summary bg-white mb-3 mb-lg-4 p-3 p-lg-4">
                      <div className="card">
                        <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                          <h4 className="title-large-semi-bold mb-3">{t('details_appointment')}</h4>
                          <Button className="text-decoration-none" onClick={() => onChangePage(4)}>
                            {t('edit')}
                          </Button>
                        </div>

                        <div className="card-body p-0">
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('reason')}</div>
                            <div className="border-light">
                              <p className="data-text">{service}</p>
                            </div>
                          </div>
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('details')}</div>
                            <div className="border-light">
                              <p className="data-text">{user_message}</p>
                            </div>
                          </div>
                        </div>
                        <div className="card-footer p-0"></div>
                      </div>
                    </div>
                    <div className="cmp-info-summary bg-white p-3 p-lg-4 mb-0">
                      <div className="card">
                        <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                          <h4 className="title-large-semi-bold mb-3">{t('applicant')}</h4>
                          <Button className="text-decoration-none" onClick={() => onChangePage(5)}>
                            {t('edit')}
                          </Button>
                        </div>

                        <div className="card-body p-0">
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('name')}</div>
                            <div className="border-light">
                              <p className="data-text">{name}</p>
                            </div>
                          </div>
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('surname')}</div>
                            <div className="border-light">
                              <p className="data-text">{surname}</p>
                            </div>
                          </div>
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('fiscal_code')}</div>
                            <div className="border-light">
                              <p className="data-text">{fiscal_code && getUUID(fiscal_code) ? '--' : fiscal_code}</p>
                            </div>
                          </div>
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('email')}</div>
                            <div className="border-light">
                              <p className="data-text">{email_address}</p>
                            </div>
                          </div>
                          <div className="single-line-info border-light">
                            <div className="text-paragraph-small">{t('phone_number')}</div>
                            <div className="border-light">
                              <p className="data-text">{phone_number}</p>
                            </div>
                          </div>
                        </div>
                        <div className="card-footer p-0"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="cmp-nav-steps">
          <nav
            className="steppers-nav"
            aria-label="Step"
            style={{
              display: 'flex',
              justifyContent: 'space-between'
            }}>
            <Button
              type="button"
              size={'sm'}
              className="steppers-btn-prev p-0"
              onClick={onPrev}
              data-focus-mouse="false">
              <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
              <span className="text-button-sm">{t('back')}</span>
            </Button>
            {!currentUser?.isAnonym && (
            <Button
              type="button"
              outline
              color="primary"
              size={'sm'}
              className="bg-white steppers-btn-save d-none d-lg-block saveBtn"
              data-focus-mouse="false"
              onClick={() => saveApplication()}>
              {t('save_request')}
            </Button>
              )}
            <Button
              color="primary"
              type="button"
              size={'sm'}
              className="steppers-btn-confirm"
              onClick={() => onSubmit({ status: '1900' })}
              disabled={isDisabled}>
              <span className="text-button-sm">{t('send')}</span>
              <Icon
                icon={'it-chevron-right'}
                size={'sm'}
                color={'primary'}
                className={'icon-white'}></Icon>
            </Button>
          </nav>
          {application?.error ? (
            <div
              id="alert-message"
              className="alert alert-danger cmp-disclaimer rounded"
              role="alert">
              <span className="d-inline-block text-uppercase cmp-disclaimer__message">
                {t('not_authorized')}
              </span>
            </div>
          ) : (
            ''
          )}
          <NotificationManager></NotificationManager>
        </div>
      </Row>
    </Container>
  );
}

export const WrapperCallout = styled.div`
.alert-warning, .message-warning, .warning {
  background-image: none;
}
  `;
