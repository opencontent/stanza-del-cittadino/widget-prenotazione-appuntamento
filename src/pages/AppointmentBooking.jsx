import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  Col,
  Container,
  Icon,
  LinkList,
  NavItem, NotificationManager, notify,
  Row,
  StepperContainer,
  StepperHeader,
  StepperHeaderElement,
  useNavScroll
} from "design-react-kit";
import { t } from 'i18next';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useScrollPercentage from 'react-scroll-percentage-hook';
import styled from "styled-components";
import Breadcrumbs from "../_components/Breadcrumbs/Breadcrumbs";
import { apiActions, authActions, currentUserActions } from '../_store';
import { PrivacyPage } from './PrivacyPage';

import { StepApplicant } from './StepApplicant';
import { StepDate } from './StepDate';
import { StepDetails } from './StepDetails';
import { StepPlace } from './StepPlace';
import { StepSummary } from './StepSummary';
import { ThankYouPage } from './ThankYouPage';
import { Trans } from 'react-i18next';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';

export { AppointmentBooking };

function AppointmentBooking() {
  const [activeStep, toggleStep] = useState(1);
  const [collapseElementOpen, setCollapseElement] = useState('1');
  const { percentage } = useScrollPercentage({ windowScroll: true });
  const dispatch = useDispatch();
  const { currentUser } = useSelector((x) => x.currentUser);

  /**NavScroll**/
  const containerRef = useRef(null);
  const { register, isActive } = useNavScroll({
    root: containerRef.current || undefined
  });
  const getActiveClass = (id) => (isActive(id) ? 'active' : undefined);
  /** **/

  const location = useLocation()
  const navigate = useNavigate()
  const { navigateStep } = location.state || {};

  const callbackOnNextPage = (step) => {
    if (!navigateStep) {
      navigate(
        { ...location },
        { state: { ...location.state, navigateStep: activeStep }, replace: true }
      );
    }
    navigate({...location}, { state: { ...location.state, navigateStep: step } });
    toggleStep(step);
  };

  useEffect(()=> {
    if(navigateStep && navigateStep < activeStep)
    toggleStep(navigateStep)
  }, [navigateStep, activeStep])

  useEffect(()=>{
    if(currentUser?.role === 'admin'){
      notify(t('warning'),t('admin_warning'), {
        dismissable: true,
        state: 'warning',
        duration: 20000
      });
    }
  },[currentUser])

  // recovery token otherwise I create it
  useEffect(() => {
    dispatch(authActions.getSessionToken()).then((el) => {
      if (el.meta.requestStatus === 'rejected') {
        dispatch(authActions.createSessionToken())
          .unwrap()
        .then(async (response) => {
          if (response?.id) {
            dispatch(currentUserActions.getUserById({id: response?.id}));
          }
          });
      } else {
        if(el.payload?.id) {
          dispatch(currentUserActions.getUserById({id: el.payload?.id}))
          .unwrap().
            then(() => {
              dispatch(apiActions.getDraftApplication());
            });
        }
      }
    });
  }, [dispatch]);

  return (
    <Container>
      {activeStep < 7 ? (
        <Row className="justify-content-center">
          <Col className="col-12" lg={10}>
            {window.OC_RENDER_BREADCRUMB === true || process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ?  <Breadcrumbs></Breadcrumbs> : null}
          </Col>
          <Col className="col-12" lg={10}>
            <div className="cmp-hero">
              <section className="it-hero-wrapper bg-white align-items-start">
                <div className="it-hero-text-wrapper pt-0 ps-0 pb-3 pb-lg-4">
                  <h1 className="text-black hero-title" data-element="page-name">
                    {t('appointment_booking')}
                  </h1>
                  <p className="subtitle-small text-black">
                    {t('report_list_description')}
                    <br />
                    <br />
                    {activeStep === 1 && currentUser?.isAnonym && (
                      <Trans
                        t={t}
                        values={{
                          url: window.OC_AUTH_URL + '?return-url=' + window.location.href
                        }}
                        i18nKey={'auth_description'}
                        components={{ Link: <Link /> }}
                      ></Trans>
                    )}
                  </p>
                </div>
              </section>
            </div>
          </Col>
          <Col className="col-12 cmp-info-progress">
            <WrapperStepsContainer>
            <StepperContainer>
              <StepperHeader>
                <StepperHeaderElement
                  variant={activeStep === 1 ? 'active' : 'confirmed'}
                  appendIcon={activeStep >= 2 ? 'it-check' : ''}
                >
                  <span className='text-uppercase'>{t('privacy_step')}</span>
                </StepperHeaderElement>
                <StepperHeaderElement
                  variant={activeStep === 2 ? 'active' : 'confirmed'}
                  appendIcon={activeStep >= 3 ? 'it-check' : ''}
                >
                  <span className='text-uppercase'>{t("place")}</span>
                </StepperHeaderElement>
                <StepperHeaderElement
                  variant={activeStep === 3 ? 'active' : 'confirmed'}
                  appendIcon={activeStep >= 4 ? 'it-check' : ''}
                >
                  <span className='text-uppercase'>{t("date_and_time")}</span>
                </StepperHeaderElement>
                <StepperHeaderElement
                  variant={activeStep === 4 ? 'active' : 'confirmed'}
                  appendIcon={activeStep >= 5 ? 'it-check' : ''}
                >
                  <span className='text-uppercase'>{t("details_appointment")}</span>
                </StepperHeaderElement>
                <StepperHeaderElement
                  variant={activeStep === 5 ? 'active' : 'confirmed'}
                  appendIcon={activeStep >= 6 ? 'it-check' : ''}
                >
                  <span className='text-uppercase'>{t("applicant")}</span>
                </StepperHeaderElement>
                <StepperHeaderElement variant={activeStep === 6 ? 'active' : 'confirmed'}>
                  <span className='text-uppercase'>{t("summary")}</span>
                </StepperHeaderElement>
                <StepperHeaderElement tag={'span'} aria-hidden="true" variant="mobile"><span>{activeStep}/6</span></StepperHeaderElement>
              </StepperHeader>
            </StepperContainer>
            </WrapperStepsContainer>
          </Col>
        </Row>
      ) : null}
      <Row className={`${activeStep === 6 ? 'justify-content-center' : ''}`}>
        {activeStep > 1 && activeStep < 6 ? (
          <div className="col-12 col-lg-3 d-lg-block mb-4 d-none">
            <div className="cmp-navscroll sticky-top" aria-labelledby="accordion-title-one">
              <nav className="navbar it-navscroll-wrapper navbar-expand-lg" aria-label={t('info')}>
                <div className="navbar-custom" id="navbarNavProgress">
                  <div className="menu-wrapper">
                    <div className="link-list-wrapper">
                      <Accordion>
                        <AccordionItem>
                          <AccordionHeader
                            id="accordion-title-one"
                            className="pb-10 px-3"
                            active={collapseElementOpen === '1'}
                            onToggle={() =>
                              setCollapseElement(collapseElementOpen !== '1' ? '1' : '')
                            }
                          >
                            {' '}
                            {t('info')}{' '}
                            <Icon icon={'it-expand'} size={'xs'} className={'right'}></Icon>
                          </AccordionHeader>{' '}
                          <div className="progress">
                            <div
                              className="progress-bar it-navscroll-progressbar"
                              role="progressbar"
                              aria-valuenow={percentage.vertical}
                              aria-valuemin="0"
                              aria-valuemax="100"
                              style={{
                                width: percentage.vertical + '%'
                              }}
                            ></div>
                          </div>
                          <AccordionBody active={collapseElementOpen === '1'}>
                            <div
                              id="collapse-one"
                              className="accordion-collapse collapse show"
                              role="region"
                              aria-labelledby="accordion-title-one"
                            >
                              <div className="accordion-body">
                                <LinkList data-element="page-index">
                                  {activeStep === 1 ? (
                                    <NavItem>
                                      <HashLink
                                        smooth
                                        className={getActiveClass('privacy')}
                                        to="#privacy"
                                      >
                                        <span className="title-medium">{t('privacy_step')}</span>
                                      </HashLink>
                                    </NavItem>
                                  ) : (
                                    ''
                                  )}
                                  {activeStep === 2 ? (
                                    <NavItem>
                                      <HashLink
                                        smooth
                                        className={getActiveClass('office')}
                                        to="#office"
                                      >
                                        <span className="title-medium">{t('office')}</span>
                                      </HashLink>
                                    </NavItem>
                                  ) : (
                                    ''
                                  )}
                                  {activeStep === 3 ? (
                                    <>
                                      <NavItem>
                                        <HashLink
                                          smooth
                                          className={getActiveClass('appointment-available')}
                                          to="#appointment-available"
                                        >
                                          <span className="title-medium">{t('appointments_available')}</span>
                                        </HashLink>
                                      </NavItem>
                                      <NavItem>
                                        <HashLink
                                          smooth
                                          className={getActiveClass('office')}
                                          to="#office"
                                        >
                                          <span className="title-medium">{t('office')}</span>
                                        </HashLink>
                                      </NavItem>
                                    </>
                                  ) : (
                                    ''
                                  )}
                                  {activeStep === 4 ? (
                                    <>
                                      <NavItem>
                                        <HashLink
                                          smooth
                                          className={getActiveClass('reason')}
                                          to="#reason"
                                        >
                                          <span className="title-medium">{t('reason')}</span>
                                        </HashLink>
                                      </NavItem>
                                      <NavItem>
                                        <HashLink
                                          smooth
                                          className={getActiveClass('details')}
                                          to="#details"
                                        >
                                          <span className="title-medium">{t('details')}</span>
                                        </HashLink>
                                      </NavItem>
                                    </>
                                  ) : (
                                    ''
                                  )}
                                  {activeStep === 5 ? (
                                    <>
                                      <NavItem>
                                        <HashLink
                                          smooth
                                          className={getActiveClass('reason')}
                                          to="#reason"
                                        >
                                          <span className="title-medium">{t('applicant')}</span>
                                        </HashLink>
                                      </NavItem>
                                    </>
                                  ) : (
                                    ''
                                  )}
                                </LinkList>
                              </div>
                            </div>
                          </AccordionBody>
                        </AccordionItem>
                      </Accordion>
                    </div>
                  </div>
                </div>
              </nav>
            </div>
          </div>
        ) : null}
        <div
          className={`col-12 ${activeStep > 1 ? 'col-lg-8' : 'col-lg-12'} ${
            activeStep > 1 && activeStep < 5 ? 'offset-lg-1' : ''
          }`}
        >
          {activeStep === 1 ? (
            <PrivacyPage
              onClick={callbackOnNextPage}
              register={{ ...register('privacy') }}
              activeStep={activeStep}
            ></PrivacyPage>
          ) : null}
          {activeStep === 2 ? (
            <StepPlace
              onClick={callbackOnNextPage}
              register={{ ...register('office') }}
              activeStep={activeStep}
            ></StepPlace>
          ) : null}
          {activeStep === 3 ? (
            <StepDate
              onClick={callbackOnNextPage}
              register={{ ...register('office') }}
              registerAppointment={{ ...register('appointment-available') }}
              activeStep={activeStep}
            ></StepDate>
          ) : null}
          {activeStep === 4 ? (
            <StepDetails
              onClick={callbackOnNextPage}
              registerReason={{ ...register('reason') }}
              registerDetails={{ ...register('details') }}
              activeStep={activeStep}
            ></StepDetails>
          ) : null}
          {activeStep === 5 ? (
            <StepApplicant
              onClick={callbackOnNextPage}
              registerApplicant={{ ...register('applicant') }}
              activeStep={activeStep}
            ></StepApplicant>
          ) : null}
          {activeStep === 6 ? (
            <StepSummary onClick={callbackOnNextPage} activeStep={activeStep}></StepSummary>
          ) : null}
          {activeStep === 7 ? <ThankYouPage onClick={callbackOnNextPage}></ThankYouPage> : null}
        </div>
        <NotificationManager></NotificationManager>
      </Row>
    </Container>
  );
}


export const WrapperStepsContainer = styled.div`
    @media (min-width: 992px) {
        .steppers-header ul {
            padding: 0;
            box-shadow: none;
            height: auto;
            display: flex;
            justify-content: space-between;
            background: rgba(0, 0, 0, 0);
            width: 100%;
        }

        .steppers .steppers-index {
            display: none;
        }
    }

        @media (max-width: 992px) {
            .steppers-header ul{
                display: contents;
            }
        }
        .steppers .steppers-index {
            margin-left: auto;
            font-size: 0.875rem;
            font-weight: 600;
            flex-shrink: 0;
        }
`
