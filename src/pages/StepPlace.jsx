import dayjs from 'dayjs';
import {
  Button,
  Container,
  Icon,
  Input,
  Label,
  Row,
  Spinner
} from "design-react-kit";
import { t } from 'i18next';
import { useEffect, useLayoutEffect, useState } from "react";
import { Controller, useForm } from 'react-hook-form';
import { getI18n } from "react-i18next";
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useSearchParams } from "react-router-dom";
import Select from 'react-select';
import styled from 'styled-components';
import DOMPurify from 'dompurify';

import {
  apiActions,
  storeCalendar, storeDay,
  storeLocation, storeMonth,
  storeOffice,
  storeOpeningHours, storeTime
} from "../_store";

import weekday from 'dayjs/plugin/weekday';

import isoWeek from 'dayjs/plugin/isoWeek';
require('dayjs/locale/en')
require('dayjs/locale/de')

dayjs.extend(weekday);
dayjs.extend(isoWeek);

export { StepPlace };

function StepPlace({ onClick, activeStep, register }) {
  const dispatch = useDispatch();
  const { offices, application } = useSelector((x) => x.api);
  const { office, opening_hour, location } = useSelector((x) => x.form);
  const [searchParams] = useSearchParams();
  const [loader, setLoader] = useState();
  const locationWindow = useLocation();
  const { currentUser } = useSelector((x) => x.currentUser);
  const {
    handleSubmit,
    formState: { errors, isDirty, isValid, isSubmitting },
    control,
    setValue
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      office: ''
    }
  });

  const onSubmit = (data) => {
    if (isSubmitting) {
      return;
    }
    dispatch(storeOffice(data.office));
    onClick(activeStep + 1);
  };

  const onPrev = () => {
    onClick(activeStep - 1);
  };

  const optionsCalendars = () => {
    if (offices?.length > 0) {
      return offices;
    }
    return [];
  };

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [locationWindow.pathname]);

  useEffect(() => {
    if (searchParams.get('user_group_id')) {
      dispatch(apiActions.getUserGroupById({ id: searchParams.get('user_group_id') })).then(
        (response) => {
          // if there is only one office, I select it and skip the step
          if (response.payload && office === null) {
            let cloneOffices = { ...response.payload };
            handleChangeOffice(cloneOffices, true);
          }
        }
      );
    } else {
      if (searchParams.get('service_id')) {
        dispatch(
          apiActions.getUserGroupsServiceId({
            service_id: searchParams.get('service_id')
          })
        ).then((response) => {
          // if there is only one office, I select it and skip the step
          if (response.payload.length === 1 && office === null) {
            let cloneOffices = { ...response.payload[0] };
            handleChangeOffice(cloneOffices, true);
          }
        });
      } else {
        dispatch(apiActions.getUserGroups());
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, searchParams, office]);

  // get draft application
  useEffect(() => {
    if (application?.data && currentUser?.role !== 'admin') {
      if (application?.data?.hasOwnProperty('user_group')) {
        handleChangeOffice(application?.data?.user_group);
      }
    }else if(office){
      setValue('office', office, {
        shouldValidate: true,
        shouldDirty: true
      });
    }else{
      if(offices.length > 0){
       // handleChangeOffice(offices[0]);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setValue, application.data,offices.length]);

  useEffect(() => {
    if (office) {
      setValue('office', office, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
  }, [office, setValue]);

  /*** Autocomplete select office if exist user_group_id param ***/
  /*   useEffect(() => {
    if(searchParams.get('user_group_id') && offices.length > 0){
      let cloneOffices = {...offices[0]}
      cloneOffices.label = cloneOffices.name;
      cloneOffices.value = cloneOffices.id;
      handleChangeOffice(cloneOffices)
      setValue("office", cloneOffices, {
        shouldValidate: true,
        shouldDirty: true,
      });
    }
  },[offices]) */

  const handleChangeOffice = (event, skip = false) => {
    setLoader(true)
    dispatch(storeOffice(event));
    //Rest month
    dispatch(storeMonth(null));
    dispatch(storeDay(null));
    dispatch(storeTime(null));

    dispatch(apiActions.getCalendarById({ calendar_id: event.calendar_id })).then((res) => {
      dispatch(storeCalendar(res.payload));
      dispatch(storeLocation(res.payload.location));
      dispatch(apiActions.getOpeningHours({ calendar_id: event.calendar_id })).then((response) => {
        if (response.payload.results.length > 0) {
          dispatch(storeOpeningHours(response.payload.results));
          if (skip) {
            handleSubmit(onSubmit)();
          }
          setLoader(false)
        }
      });
      setLoader(false)
    });
  };

  return (
    <Container>
      <Row>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="steppers-content" aria-live="polite">
            <div className="it-page-sections-container">
              <section className="it-page-section" id="office" {...register}>
                <div className="cmp-card">
                  <div className="card has-bkg-grey shadow-sm p-big">
                    <div className="card-header border-0 p-0 mb-lg-30">
                      <div className="d-flex">
                        <h2 className="title-xxlarge mb-0">{t('office')}</h2>
                      </div>
                      <p className="subtitle-small mb-0">{t('appointment_booking_description')}</p>
                    </div>
                    <div className="card-body p-0">
                      <div className="">
                        <Controller
                          name="office"
                          control={control}
                          rules={{ required: true }}
                          render={({ field }) => (
                            <WrapperClassSelect className="select-wrapper p-big bg-transparent p-0">
                              <label htmlFor="office" className="visually-hidden">
                                {t('type_office')}
                              </label>
                              <Select
                                id="office"
                                className="react-select-container"
                                classNamePrefix="react-select"
                                options={optionsCalendars()}
                                placeholder={t('select_office') + '*'}
                                aria-label={t('type_office')}
                                noOptionsMessage={() => t('no_items')}
                                invalid={errors.office?.type === 'required' ? true : false}
                                infoText={
                                  errors.office?.type === 'required' ? t('required_field') : false
                                }
                                innerRef={field.ref}
                                value={field.value}
                                getOptionLabel={(option) => option.name}
                                getOptionValue={(option) => option.id}
                                onChange={(event) => {
                                  field.onChange(event);
                                  handleChangeOffice(event);
                                }}
                              />
                            </WrapperClassSelect>
                          )}
                        />
                      </div>
                      <fieldset>
                        <legend className="visually-hidden">{t('select_user_group')}</legend>
                        {opening_hour[0]?.name && (
                          <div className="cmp-info-radio radio-card">
                            <div className="card p-3 p-lg-4">
                              <div className="card-header mb-0 p-0">
                                <div className="form-check m-0">
                                  <Input
                                    className="radio-input"
                                    name="beneficiaries"
                                    type="radio"
                                    id="third"
                                    defaultChecked
                                  />
                                  <Label htmlFor="third" className="active">
                                    <h3 className="big-title" id={'thirdDescription'}>
                                      {/* {opening_hour.name} */}
                                      {t('site')}
                                    </h3>
                                  </Label>
                                </div>
                              </div>
                              <div className="card-body p-0">
                                <div className="info-wrapper">
                                  <span className="info-wrapper__label">{t('name')}</span>
                                  <p className="info-wrapper__value">{office?.name}</p>
                                </div>
                                <div className="info-wrapper">
                                  <span className="info-wrapper__label">{t('address')}</span>
                                  <p
                                    className="info-wrapper__value"
                                    dangerouslySetInnerHTML={{
                                      __html: DOMPurify.sanitize(location)
                                    }}></p>
                                </div>
                                <span className="info-wrapper__label">{t('opening')}</span>
                                {opening_hour?.length &&
                                  opening_hour?.map((op, idx) => (
                                    <div className="info-wrapper" key={idx + '_info'}>
                                      <p className="info-wrapper__value">
                                        <b>{op?.name} </b> <br />
                                        {op?.days_of_week.map(
                                          (el, index, { length }) =>
                                            dayjs().isoWeekday(el).locale(getI18n().language).format('ddd') +
                                            (index + 1 !== length ? ', ' : ' ')
                                        )}
                                        {t('times')} {op?.begin_hour} – {op?.end_hour}
                                      </p>
                                    </div>
                                  ))}
                              </div>
                            </div>
                          </div>
                        )}
                      </fieldset>
                    </div>
                  </div>
                </div>
              </section>
              {loader && !opening_hour[0]?.name ? (
                <div className="text-center d-flex justify-content-center my-3">
                  <Spinner active size={'large'}></Spinner>
                </div>
              ) : null}
            </div>
          </div>
          <div className="cmp-nav-steps">
            <nav
              className="steppers-nav"
              aria-label="Step"
              style={{
                display: 'flex',
                justifyContent: 'space-between'
              }}>
              <Button type="button" size={'sm'} className="steppers-btn-prev p-0" onClick={onPrev}>
                <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
                <span className="text-button-sm">{t('back')}</span>
              </Button>
              <Button
                color="primary"
                type="submit"
                size={'sm'}
                className="steppers-btn-confirm"
                disabled={!opening_hour[0]?.name ? true : false && (!isValid || !isDirty)}>
                <span className="text-button-sm">{t('next')}</span>
                <Icon
                  icon={'it-chevron-right'}
                  size={'sm'}
                  color={'primary'}
                  className={'icon-white'}></Icon>
              </Button>
            </nav>
          </div>
        </form>
      </Row>
    </Container>
  );
}

export const WrapperClassSelect = styled.div`
  .react-select__value-container {
    border: none;
    border-radius: 0;
    outline: 0;
    width: 100%;
    box-shadow: none;
    font-weight: 700;
    color: #17324d;
    background-color: #fff;
    &:focus:not(.focus--mouse) {
      border-color: #000 !important;
      box-shadow: 0 0 0 3px #000 !important;
      outline: 3px solid #fff !important;
      outline-offset: 3px;
    }
  }

  .react-select__control {
    border: none;
    border-bottom: 1px solid #5b6f82;
    box-shadow: none;
    border-radius: 0;

    &:hover {
      border-color: #5b6f82;
    }
    &.react-select__control--is-focused {
      border-color: #000 !important;
      box-shadow: 0 0 0 3px #000 !important;
      outline: 3px solid #fff !important;
      outline-offset: 3px;
    }

    &.react-select__control--is-disabled {
      .react-select__value-container {
        background-color: hsl(0, 0%, 95%);
      }
    }
  }

  .react-select__input {
    height: 2rem !important;
    :focus:not(.focus--mouse) {
      box-shadow: none !important;
      outline: none !important;
      outline-offset: 3px;
    }
  }

  .react-select__indicator-separator {
    display: none;
  }

  .react-select__menu {
    z-index: 2;
  }

  .react-select__single-value {
    margin-left: 4px;
  }
  .react-select__placeholder {
    color: #596d88;
  }
`;
