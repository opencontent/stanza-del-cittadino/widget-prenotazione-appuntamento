import dayjs from 'dayjs';
import { Col, Icon, Row, Container } from 'design-react-kit';
import { t } from 'i18next';
import { useEffect } from "react";
import { getI18n, Trans } from "react-i18next";
import { useSelector } from 'react-redux';
import Breadcrumbs from "../_components/Breadcrumbs/Breadcrumbs";
export { ThankYouPage };

function ThankYouPage() {
  const { application } = useSelector((x) => x.form);
  const { is_moderated, meeting } = useSelector((x) => x.api);

  // SE clicco il pulsante back del browser ricarico la pagina
  useEffect(() => {
    window.addEventListener("popstate", () => {
     window.location.reload()
    });
  }, []);

  return (
    <Container>
      <Row className="justify-content-center">
        <Col className="col-12" lg={10}>
          {window.OC_RENDER_BREADCRUMB === true ||
          process.env.REACT_APP_OC_RENDER_BREADCRUMB === 'true' ? (
            <Breadcrumbs></Breadcrumbs>
          ) : null}
        </Col>
      </Row>
      <Row className="justify-content-center mb-50">
        <Col className="col-12" lg={10}>
          <div className="cmp-hero">
            <section className={'it-hero-wrapper bg-white align-items-start'}>
              <div className="it-hero-text-wrapper pt-0 ps-0 pb-40 pb-lg-60">
                <div className="categoryicon-top d-flex">
                  <Icon color="success" icon="it-check-circle" className={' mr-10 icon-sm mb-1'} />
                  <h1 className="text-black hero-title">
                    {is_moderated ? t('request_appointment') : t('request_sent')}
                  </h1>
                </div>
                <div className="hero-text">
                  <p className="pt-3 pt-lg-4">
                    {is_moderated ? t('appointments_moderated') : t('appointments_fixed')}{' '}
                    <strong>
                      {dayjs(application?.data?.day)
                        .locale(getI18n().language)
                        .format('dddd DD MMMM YYYY')}{' '}
                      {t('from')} {application?.data.time.start_time} {t('to')}{' '}
                      {application?.data.time.end_time}
                    </strong>
                  </p>
                  <br />
                  {meeting?.code && (
                    <>
                      <p>
                        {t('appointment_number')}
                        {': '}
                        <strong>{meeting.code}</strong>
                      </p>
                      <br />
                    </>
                  )}
                  <p>
                    <Trans t={t} i18nKey={'thanks_message_report_sent_email'}></Trans>
                    <br />
                    <strong>{application?.data.applicant.data.email_address}</strong>
                  </p>
                </div>
              </div>
            </section>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
