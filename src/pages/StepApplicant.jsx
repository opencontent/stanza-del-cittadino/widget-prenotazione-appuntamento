import dayjs from 'dayjs';
import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  Button,
  Card,
  CardBody,
  CardHeader,
  Container,
  Icon,
  Input, NotificationManager, notify,
  Row
} from "design-react-kit";
import { t } from 'i18next';
import { useEffect, useLayoutEffect, useState } from "react";
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useLocation } from "react-router-dom";
import { getUUID } from "../_helpers/utilities";
import {
  apiActions,
  storeApplication,
  storeEmail,
  storeFiscalCode,
  storeName,
  storePhoneNumber,
  storeSurname
} from '../_store';
import CodiceFiscale from 'codice-fiscale-js';

export { StepApplicant };

function StepApplicant({ onClick, registerApplicant, activeStep }) {
  const {
    name,
    surname,
    fiscal_code,
    phone_number,
    email_address,
    time,
    user_message,
    office,
    service
  } = useSelector((x) => x.form);
  const { meeting, application, service_id } = useSelector((x) => x.api);
  const { user_data } = useSelector((x) => x.auth);
  const { currentUser } = useSelector((x) => x.currentUser);
  const [collapseElementOpenCard, setCollapseElementCard] = useState('1');
  const { token } = useSelector((x) => x.auth);
  const location = useLocation();
  const dispatch = useDispatch();

  const {
    handleSubmit,
    formState: { errors, isDirty, isValid, isSubmitting },
    control,
    setValue
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      email_address: '',
      name: '',
      surname: '',
      fiscal_code: '',
      phone_number: ''
    }
  });

  useEffect(() => {
    dispatch(apiActions.getServiceBookings());

    if (token && !currentUser?.isAnonym) {
      setValue('name', currentUser.nome, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('surname', currentUser.cognome, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('email_address', currentUser.email, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('fiscal_code', currentUser.codice_fiscale, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('phone_number', currentUser.telefono || currentUser.cellulare, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
  }, [user_data, dispatch, setValue, currentUser, token]);

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);

/*   const validateEmail = (email) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }; */


  const onSubmit = (data) => {
    if (isSubmitting) {
      return;
    }
    dispatch(storeName(data.name));
    dispatch(storeSurname(data.surname));
    dispatch(storeEmail(data.email_address));
    dispatch(storePhoneNumber(data.phone_number));
    dispatch(storeFiscalCode(data.fiscal_code));
    const meetingBooking = {
      date: time.date,
      slot: time.start_time + '-' + time.end_time,
      opening_hour: time.opening_hour,
      calendar: office.calendar_id,
      meeting: meeting?.id
    };
    dispatch(apiActions.newMeeting(meetingBooking)).unwrap().then((e) => {
      onClick(activeStep + 1);
    }).catch(e => {
      notify(t('request_saved_error'), {
        dismissable: true,
        state: 'error',
        duration: 10000
      });
    });
  };

  const onPrev = () => {
    onClick(activeStep - 1);
  };

  useEffect(() => {
    if (name && surname) {
      setValue('name', name, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('surname', surname, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('email_address', email_address, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('phone_number', phone_number, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('fiscal_code', fiscal_code, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
  }, [name, surname, fiscal_code, phone_number, email_address, setValue,errors]);

  const saveApplication = () => {
    const getId =
      application?.id || application?.data?.length
        ? application?.id || application?.data[0]?.id
        : null;
    const dataApplication = {
      ...(getId && { id: getId }),
      service: service_id?.identifier,
      data: {
        applicant: {
          data: {
            email_address: email_address || currentUser.email,
            phone_number: phone_number || currentUser.telefono || currentUser.cellulare,
            completename: {
              data: {
                name: name || currentUser.nome,
                surname: surname || currentUser.cognome
              }
            },
            fiscal_code: {
              data: {
                fiscal_code: fiscal_code || currentUser.codice_fiscale
              }
            }
          }
        },
        calendar: `${dayjs(time.date).format('DD/MM/YYYY')} @ ${time.start_time}-${
          time.end_time
        } (${office.calendar_id}#${meeting.id}#${time.opening_hour})`,
        user_group: {
          id: office.id,
          name: office.name,
          calendar_id: office.calendar_id
        },
        day: time.date,
        meeting_id: meeting.id,
        service: service,
        time: {
          availability: true,
          date: time.date,
          end_time: time.end_time,
          opening_hour: time.opening_hour,
          slots_available: 1,
          start_time: time.start_time
        },
        user_message: user_message
      },
      status: '1000'
    };

    dispatch(storeApplication(dataApplication));

    if (dataApplication?.id) {
      return dispatch(
        apiActions.updateApplication({
          data: dataApplication,
          id: dataApplication.id
        })
      ).then(() => {
        notify(t('request_saved_successfully'), {
          dismissable: true,
          state: 'success',
          duration: 3000
        });
      });
    } else {
      return dispatch(apiActions.confirmMeeting(dataApplication)).then(() => {
        notify(t('request_saved_error'), {
          dismissable: true,
          state: 'success',
          duration: 3000
        });
      });
    }
  };


  const checkIsValidCF = (val) => {
    if (currentUser?.nome !== 'Anonymous') {
      if (CodiceFiscale.check(val)) {
        return true
      } else {
        return t('fiscal_code_validator');
      }
    } else {
      if(val){
        if (CodiceFiscale.check(val)) {
          return true
        } else {
          return t('fiscal_code_validator');
        }
      }
    }
  }

  return (
    <Container>
      <Row>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="steppers-content" aria-live="polite">
            <div className="it-page-sections-container">
              {!currentUser?.isAnonym ? (
                <section className="it-page-section" id="applicant" {...registerApplicant}>
                  <div className="cmp-card navbar-custom">
                    <div className="card has-bkg-grey shadow-sm">
                      <div className="card-header border-0 p-0 mb-lg-30 m-0">
                        <div className="d-flex">
                          <h2 className="title-xxlarge mb-1">{t('applicant')}</h2>
                        </div>
                        <p className="subtitle-small mb-0">{t('my_info')}</p>
                      </div>
                      <div className="card-body p-0">
                        <div className="cmp-info-button-card mt-3">
                          <div className="card p-3 p-lg-4">
                            <div className="card-body p-0">
                              <h3 className="big-title mb-0">
                                {currentUser?.nome ? currentUser?.nome : '--'}{' '}
                                {currentUser?.cognome}
                              </h3>

                              <p className="card-info">
                                {t('fiscal_code')} <br />
                                <span>
                                  {currentUser?.codice_fiscale && getUUID(currentUser?.codice_fiscale) ? '--' : currentUser?.codice_fiscale}
                                </span>
                              </p>
                              <Accordion>
                                <AccordionItem>
                                  <AccordionHeader
                                    active={collapseElementOpenCard === '1'}
                                    onToggle={() =>
                                      setCollapseElementCard(
                                        collapseElementOpenCard !== '1' ? '1' : ''
                                      )
                                    }>
                                    {collapseElementOpenCard !== '1'
                                      ? t('show_more')
                                      : t('show_less')}
                                    <Icon icon={'it-expand'} size={'sm'} color={'primary'}></Icon>
                                  </AccordionHeader>
                                  <AccordionBody
                                    className="accordion-collapse-custom"
                                    active={collapseElementOpenCard === '1'}
                                    listClassName={'p-0'}>
                                    <div className="cmp-info-summary bg-white has-border">
                                      <Card>
                                        <CardHeader className="border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                                          <h4 className="title-large-semi-bold mb-3">
                                            {t('contacts')}
                                          </h4>
                                          <Link
                                            to="_components/Card/CardPost#q"
                                            className="d-none text-decoration-none">
                                            {t('edit')}
                                          </Link>
                                        </CardHeader>
                                        <CardBody className="p-0">
                                            <Controller
                                              name="phone_number"
                                              control={control}
                                              rules={{
                                                required: {
                                                  value: true,
                                                  message: t('required_field')
                                                },
                                                pattern: {
                                                  value: /^(?:(\+|00)(?:\d{1,3}\s?)?(?:\d{1,3}\s?))?((?:[1-9](?:[0-9]{1,4})(?:\s?)(?:\d{3,5})(?:\s?)(?:\d{3,5}))|(?:0(?:[0-9]{1,4})(?:\s?)(?:\d{4,13})))$/,
                                                  message: t('phone_number_validator')
                                                }
                                              }}
                                              render={({ field }) => (
                                                <Input
                                                  id={'phone_number'}
                                                  type="text"
                                                  className={'cmp-input mb-0'}
                                                  label={t('phone_number') + '*'}
                                                  placeholder={''}
                                                  invalid={
                                                    errors.phone_number?.type === 'required' ||
                                                    errors.phone_number?.type === 'pattern'
                                                      ? true
                                                      : false
                                                  }
                                                  validationText={
                                                    errors.phone_number?.message || t('phone_number_description')
                                                  }
                                                  innerRef={field.ref}
                                                  value={field.value}
                                                  onChange={field.onChange}
                                                />
                                              )}
                                            />
                                            <Controller
                                              name="email_address"
                                              control={control}
                                              rules={{
                                                required: true,
                                                pattern: {
                                                  value:
                                                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                  message: t('email_validator')
                                                }
                                              }}
                                              render={({ field }) => (
                                                <Input
                                                  id={'email_address'}
                                                  type="email"
                                                  className={'cmp-input mb-0'}
                                                  label={t('email') + '*'}
                                                  placeholder={''}
                                                  invalid={
                                                    errors.email_address?.type === 'required' ||
                                                    errors.email_address?.type === 'pattern'
                                                      ? true
                                                      : false
                                                  }
                                                  infoText={
                                                    errors.email_address?.type === 'required' ||
                                                    errors.email_address?.type === 'pattern'
                                                      ? t('email_validator')
                                                      : t('email_description')
                                                  }
                                                  innerRef={field.ref}
                                                  value={field.value}
                                                  onChange={field.onChange}
                                                />
                                              )}
                                            />
                                        </CardBody>
                                      </Card>
                                    </div>
                                  </AccordionBody>
                                </AccordionItem>
                              </Accordion>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              ) : (
                <section className="it-page-section" id="applicant" {...registerApplicant}>
                  <div className="cmp-card mb-40">
                    <div className="card has-bkg-grey shadow-sm p-big">
                      <div className="card-header border-0 p-0 mb-lg-30 m-0">
                        <div className="d-flex">
                          <h2 className="title-xxlarge mb-3">{t('applicant')}</h2>
                        </div>
                      </div>
                      <div className="card-body p-0">
                        <div className="form-wrapper bg-white p-4">
                          <Controller
                            name="name"
                            control={control}
                            rules={{ required: {
                                value: true,
                                message: t('required_field')
                              } }}
                            render={({ field }) => (
                              <Input
                                id={'name'}
                                type="text"
                                className={'cmp-input mb-0'}
                                label={t('name') + '*'}
                                placeholder={''}
                                invalid={errors.name?.type === 'required' ? true : false}
                                validationText={
                                  errors.name?.message || t('name_description')
                                }
                                innerRef={field.ref}
                                value={field.value}
                                onChange={field.onChange}
                              />
                            )}
                          />
                          <Controller
                            name="surname"
                            control={control}
                            rules={{ required: {
                                value: true,
                                message: t('required_field')
                              } }}
                            render={({ field }) => (
                              <Input
                                id={'surname'}
                                type="text"
                                className={'cmp-input mb-0'}
                                label={t('surname') + '*'}
                                placeholder={''}
                                invalid={errors.surname?.type === 'required' ? true : false}
                                validationText={
                                  errors.surname?.message || t('surname_description')
                                }
                                innerRef={field.ref}
                                value={field.value}
                                onChange={field.onChange}
                              />
                            )}
                          />

                          <Controller
                            name="fiscal_code"
                            control={control}
                            rules={{
                              validate: checkIsValidCF, // Usa la funzione custom
                            }}
                            render={({ field }) => (
                              <>
                                <Input
                                  id={'fiscal_code'}
                                  type="text"
                                  className={'cmp-input mb-0'}
                                  label={t('fiscal_code')}
                                  placeholder={''}
                                  invalid={errors.fiscal_code?.type === 'validate'}
                                  validationText={
                                    errors.fiscal_code?.message || t('fiscal_code_description')
                                  }
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={(event) => {
                                    setValue('fiscal_code', event.target.value.toUpperCase());
                                    field.onChange(event);
                                  }}
                                />
                              </>
                            )}
                          />
                          <Controller
                            name="email_address"
                            control={control}
                            rules={{
                              required: {
                                value: true,
                                message: t('required_field')
                              },
                              pattern: {
                                value:
                                  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                message: t('email_validator')
                              }
                            }}
                            render={({ field }) => (
                              <Input
                                id={'email_address'}
                                type="email"
                                className={'cmp-input mb-0'}
                                label={t('email') + '*'}
                                placeholder={''}
                                invalid={
                                  errors.email_address?.type === 'required' ||
                                  errors.email_address?.type === 'pattern'
                                    ? true
                                    : false
                                }
                                validationText={
                                  errors.email_address?.message ||  t('email_description')
                                }
                                innerRef={field.ref}
                                value={field.value}
                                onChange={field.onChange}
                              />
                            )}
                          />
                          <Controller
                            name="phone_number"
                            control={control}
                            rules={{
                              required: {
                                value: true,
                                message: t('required_field')
                              },
                              pattern: {
                                value: /^(?:(\+|00)(?:\d{1,3}\s?)?(?:\d{1,3}\s?))?((?:[1-9](?:[0-9]{1,4})(?:\s?)(?:\d{3,5})(?:\s?)(?:\d{3,5}))|(?:0(?:[0-9]{1,4})(?:\s?)(?:\d{4,13})))$/,
                                message: t('phone_number_validator')
                              }
                            }}
                            render={({ field }) => (
                              <Input
                                id={'phone_number'}
                                type="text"
                                className={'cmp-input mb-0'}
                                label={t('phone_number') + '*'}
                                placeholder={''}
                                invalid={
                                  errors.phone_number?.type === 'required' ||
                                  errors.phone_number?.type === 'pattern'
                                    ? true
                                    : false
                                }
                              validationText={
                              errors.phone_number?.message || t('phone_number_description')
                            }
                                innerRef={field.ref}
                                value={field.value}
                                onChange={field.onChange}
                              />
                            )}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              )}
            </div>
          </div>
          <div className="cmp-nav-steps">
            <nav
              className="steppers-nav"
              aria-label="Step"
              style={{
                display: 'flex',
                justifyContent: 'space-between'
              }}>
              <Button type="button" size={'sm'} className="steppers-btn-prev p-0" onClick={onPrev}>
                <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
                <span className="text-button-sm">{t('back')}</span>
              </Button>
              {!currentUser?.isAnonym && (
                <Button
                  type="button"
                  outline
                  color="primary"
                  size={'sm'}
                  className="bg-white steppers-btn-save saveBtn"
                  data-focus-mouse="false"
                  onClick={() => saveApplication()}>
                  {t('save_request')}
                </Button>
              )}
              <Button
                color="primary"
                type="submit"
                size={'sm'}
                className="steppers-btn-confirm"
                disabled={!isValid || !isDirty}>
                <span className="text-button-sm">{t('next')}</span>
                <Icon
                  icon={'it-chevron-right'}
                  size={'sm'}
                  color={'primary'}
                  className={'icon-white'}></Icon>
              </Button>
            </nav>
          </div>
          <NotificationManager></NotificationManager>
        </form>
      </Row>
    </Container>
  );
}
