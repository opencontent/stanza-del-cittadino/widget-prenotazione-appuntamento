import dayjs from 'dayjs';
import { Button, Container, Icon, Row } from 'design-react-kit';
import  { t } from 'i18next';
import { useEffect, useLayoutEffect, useState } from "react";
import { Controller, useForm } from 'react-hook-form';
import { getI18n } from "react-i18next";
import { useSelector, useDispatch } from 'react-redux';
import { useLocation } from "react-router-dom";
import Select from 'react-select';

import { apiActions, storeCalendar, storeDay, storeMonth, storeTime } from '../_store';
import { WrapperClassSelect } from './StepPlace';

require('dayjs/locale/de')
require('dayjs/locale/en')

export { StepDate };

function StepDate({ onClick, register, registerAppointment, activeStep }) {
  const dispatch = useDispatch();
  const { availabilities, times, months, application } = useSelector((x) => x.api);
  const { office, day, time, month, opening_hour, calendar, location } = useSelector((x) => x.form);
  const [detectChangeMonth, setDetectChangeMonth] = useState(true);
  const locationPath = useLocation();

  /*** Load months availabilities ***/
  useEffect(() => {
    if (office.calendar_id) {
      dispatch(apiActions.getCalendarMonths({ calendar_id: office.calendar_id }));
    }
  }, [office.calendar_id, dispatch]);

  const {
    handleSubmit,
    formState: { errors, isDirty, isValid, isSubmitting },
    control,
    resetField,
    setValue,
    getValues
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      day: '',
      time: '',
      month: ''
    }
  });

  // get draft application
  useEffect(() => {
    if (application?.data) {
      if (application?.data?.hasOwnProperty('service')) {
        setValue('service', application?.data?.service, {
          shouldValidate: true,
          shouldDirty: true
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setValue, application.data]);

  const onSubmit = (data) => {
    if (isSubmitting) {
      return;
    }
    dispatch(storeDay(data.day));
    dispatch(storeTime(data.time));
    dispatch(
      storeCalendar(
        `${data.time.start_time}-${data.time.end_time} (${office.value}#${data.time.opening_hour})`
      )
    );
    onClick(activeStep + 1);
  };

  const onPrev = () => {
    onClick(activeStep - 1);
  };

  //List next 12 months
  /*   const optionsCalendarMonths = () => {
    const now = new Date();
    let month = now.getMonth();
    let year = now.getFullYear();

    const months = [...Array(12).keys()].map((key) =>
      new Date(0, key).toLocaleString("it-IT", { month: "long" })
    );

    let res = [];
    for (let i = 0; i < 12; ++i) {
      let from_time = new Date(year, month)
      let to_time = new Date(year, month +1,0)
      res.push({
        value: month,
        label: months[month] + " " + year,
        from_time: dayjs(from_time).format('YYYY-MM-DD'),
        to_time: dayjs(to_time).format('YYYY-MM-DD'),
      })

      if (++month === 12) {
        month = 0;
        ++year;
      }
    }
    return res || [];
  }; */

  // Scroll to top if path changes
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [locationPath.pathname]);

  const optionsCalendarMonths = () => {
    if (months?.length > 0) {
      const monthsList = months.map((key) => dayjs(key.date).startOf('month').format('YYYY-MM-DD'));
      let uniqueMonths = [...new Set(monthsList)];

      return uniqueMonths.map((key) => {
        let from_time = dayjs(key).startOf('month');
        let to_time = dayjs(key).endOf('month');
        return {
          value: dayjs(key).format('MM'),
          label: dayjs(key).locale(getI18n().language).format('MMMM YYYY'),
          from_time: dayjs(from_time).format('YYYY-MM-DD'),
          to_time: dayjs(to_time).format('YYYY-MM-DD')
        };
      });
    }
    return [];
  };
  const optionsCalendarAvailabilities = () => {
    const options = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    };
    if (availabilities?.length > 0) {
      // Change key items for select input pattern
      return availabilities.map((el) => ({
        value: el.date,
        label: new Date(el.date).toLocaleDateString(getI18n().language, options)
      }));
    }
    return [];
  };

  const optionsCalendarAvailabilitiesTime = () => {
    if (times?.length > 0) {
      // Change key items for select input pattern
      return times.map((el) => ({
        value: el.opening_hour,
        label: el.start_time + ' - ' + el.end_time,
        ...el
      }));
    }
    return [];
  };

  useEffect(() => {
    if (day && time) {
      setValue('day', day, {
        shouldValidate: true,
        shouldDirty: true
      });
      setValue('time', time, {
        shouldValidate: true,
        shouldDirty: true
      });
    }

    if (month) {
      setValue('month', month, {
        shouldValidate: true,
        shouldDirty: true
      });
    }
  }, [day, time, month, setValue]);

  const handleChangeMonth = (event) => {
    dispatch(storeMonth(event));
    if (office) {
      resetField('day');
      resetField('time');
      dispatch(
        apiActions.getCalendarAvailabilities({
          from_time: event.from_time,
          to_time: event.to_time,
          id: office.calendar_id
        })
      ).unwrap().then( (response) => {
        if(response.length){
          setDetectChangeMonth(true)
        }else {
          setDetectChangeMonth(false)
        }

      });
    }
  };

  const handleChangeDay = () => {
    if (getValues('day').value) {
      resetField('time');
      dispatch(
        apiActions.getCalendarAvailabilitiesTime({
          calendar_id: office.calendar_id,
          day: getValues('day').value
        })
      )
    }
  };

  const handleChangeTime = () => {
    if (getValues('time').value && office.calendar_id) {
      //Set is_moderated state
      dispatch(apiActions.getOpeningHourById({calendar_id: office.calendar_id, id:getValues('time').value} ))
    }
  };



  return (
    <Container>
      <Row>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="steppers-content" aria-live="polite">
            <div className="it-page-sections-container">
              <section
                className="it-page-section"
                id="appointment-available"
                {...registerAppointment}
              >
                <div className="cmp-card">
                  <div className="card has-bkg-grey shadow-sm p-big">
                    <div className="card-header border-0 p-0 mb-lg-30">
                      <div className="d-flex">
                        <h2 className="title-xxlarge mb-0">{t('appointments_available')}*</h2>
                      </div>
                    </div>
                    <div className="card-body p-0">
                      <div className="">
                        <Controller
                          name="month"
                          control={control}
                          rules={{ required: true }}
                          render={({ field }) => (
                            <WrapperClassSelect className="select-wrapper p-0 mt-1 select-partials">
                              <label htmlFor="month" className="visually-hidden">
                                {t('day_selected')}
                              </label>
                              <Select
                                id="month"
                                className="react-select-container"
                                classNamePrefix="react-select"
                                options={optionsCalendarMonths()}
                                placeholder={t('month_selected')}
                                aria-label={t('month_selected')}
                                noOptionsMessage={() => t('no_items')}
                                invalid={errors.month?.type === 'required' ? true : false}
                                infoText={
                                  errors.month?.type === 'required' ? t('required_field') : false
                                }
                                innerRef={field.ref}
                                value={field.value}
                                onChange={(event) => {
                                  field.onChange(event);
                                  handleChangeMonth(event);
                                }}
                              />
                            </WrapperClassSelect>
                          )}
                        />
                      </div>

                      <div className="">
                        <Controller
                          name="day"
                          control={control}
                          rules={{ required: true }}
                          render={({ field }) => (
                            <WrapperClassSelect className="select-wrapper p-0 mt-4 select-partials">
                              <label htmlFor="day" className="visually-hidden">
                                {t('day_selected')}
                              </label>
                              <Select
                                id="day"
                                className="react-select-container"
                                classNamePrefix="react-select"
                                noOptionsMessage={() => t('no_items')}
                                options={optionsCalendarAvailabilities()}
                                placeholder={t('day_selected')}
                                aria-label={t('day_selected')}
                                invalid={errors.day?.type === 'required' ? true : false}
                                infoText={
                                  errors.day?.type === 'required' ? t('required_field') : false
                                }
                                innerRef={field.ref}
                                value={field.value}
                                isDisabled={!months || availabilities?.length === 0}
                                onChange={(event) => {
                                  field.onChange(event);
                                  handleChangeDay(event);
                                }}
                              />
                            </WrapperClassSelect>
                          )}
                        />
                        {getValues('month')?.value && availabilities?.length === 0 && !detectChangeMonth  ? (
                          <small className="invalid-feedback d-block">
                            {t('no_day_available')}
                          </small>
                        ) : (
                          ''
                        )}
                      </div>
                      <div className="">
                        <Controller
                          name="time"
                          control={control}
                          rules={{ required: true }}
                          render={({ field }) => (
                            <WrapperClassSelect className="select-wrapper p-0 mt-4 select-partials">
                              <label htmlFor="time" className="visually-hidden">
                                {t('time_selected')}
                              </label>
                              <Select
                                id="time"
                                className="react-select-container"
                                classNamePrefix="react-select"
                                options={optionsCalendarAvailabilitiesTime()}
                                getOptionValue={(option) => option.label}
                                placeholder={t('time_selected')}
                                aria-label={t('time_selected')}
                                noOptionsMessage={() => t('no_items')}
                                invalid={errors.time?.type === 'required' ? true : false}
                                infoText={
                                  errors.time?.type === 'required' ? t('required_field') : false
                                }
                                innerRef={field.ref}
                                value={field.value}
                                onChange={(event) => {
                                  field.onChange(event);
                                  handleChangeTime(event);
                                }}

                                isDisabled={!availabilities?.length}
                              />
                            </WrapperClassSelect>
                          )}
                        />

                        {availabilities?.length > 0 &&  time?.length === 0 && times?.length === 0 ? (
                          <small className="invalid-feedback d-block">
                            {t('no_time_available')}
                          </small>
                        ) : (
                          ''
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {office ? (
                <section className="it-page-section" id="office" {...register}>
                  <div className="cmp-card">
                    <div className="card has-bkg-grey shadow-sm p-big">
                      <div className="card-header border-0 p-0 mb-lg-30">
                        <div className="d-flex">
                          <h2 className="title-xxlarge mb-0">{t('office')}</h2>
                        </div>
                      </div>
                      <div className="card-body p-0">
                        <div className="cmp-info-summary bg-white mb-4 mb-lg-30 p-4">
                          <div className="card">
                            <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                              <h4 className="title-large-semi-bold mb-3"> {t('site')}</h4>
                            </div>

                            <div className="card-body p-0">
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('name')}</div>
                                <div className="border-light">
                                  <p className="data-text">{office?.name}</p>
                                </div>
                              </div>
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('address')}</div>
                                <div className="border-light">
                                  <p
                                    className="data-text"
                                    dangerouslySetInnerHTML={{
                                      __html: calendar.location || location
                                    }}
                                  ></p>
                                </div>
                              </div>
                              <div className="border-light">
                                <div className="text-paragraph-small mt-3">{t('opening')}</div>
                                {opening_hour?.length && opening_hour.map( (op, idx) => (
                                <div className="border-light" key={idx +'_info'}>
                                  {op?.days_of_week?.length ? (
                                    <p className="data-text single-line-info border-light">
                                      <b>{op?.name}{' '}</b> <br/>
                                      {op?.days_of_week.map(
                                        (el, index, { length }) =>
                                          dayjs().isoWeekday(el).locale(getI18n().language).format('ddd') +
                                          (index + 1 !== length ? ', ' : ' ')
                                      )}
                                      {t('times')} {op?.begin_hour} –{' '}
                                      {op?.end_hour}
                                    </p>
                                  ) : null}
                                </div>
                                ))}
                              </div>
                            </div>
                            <div className="card-footer p-0"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              ) : (
                ''
              )}
            </div>
          </div>
          <div className="cmp-nav-steps">
            <nav className="steppers-nav" aria-label="Step" style={{display:'flex',justifyContent:'space-between'}}>
              <Button type="button" size={'sm'} className="steppers-btn-prev p-0" onClick={onPrev}>
                <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
                <span className="text-button-sm">{t('back')}</span>
              </Button>
              <Button
                color="primary"
                type="submit"
                size={'sm'}
                className="steppers-btn-confirm"
                disabled={!isValid || !isDirty}
              >
                <span className="text-button-sm">{t('next')}</span>
                <Icon
                  icon={'it-chevron-right'}
                  size={'sm'}
                  color={'primary'}
                  className={'icon-white'}
                ></Icon>
              </Button>
            </nav>
          </div>
        </form>
      </Row>
    </Container>
  );
}
